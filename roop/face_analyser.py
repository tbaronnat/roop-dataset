import threading
from typing import Any, Optional, List
import insightface
import numpy
import cv2
import os
import roop.globals
from roop.typing import Frame, Face
from roop.utilities import is_image
from facenet_pytorch import MTCNN, InceptionResnetV1
from roop.utilities import np_cos_angle
from PIL import Image
import torch

FACE_ANALYSER = None
THREAD_LOCK = threading.Lock()


def get_face_analyser() -> Any:
    global FACE_ANALYSER

    with THREAD_LOCK:
        if FACE_ANALYSER is None:
            FACE_ANALYSER = insightface.app.FaceAnalysis(name='buffalo_l', providers=roop.globals.execution_providers)
            FACE_ANALYSER.prepare(ctx_id=0)
    return FACE_ANALYSER


def clear_face_analyser() -> Any:
    global FACE_ANALYSER

    FACE_ANALYSER = None


def get_one_face(frame: Frame, position: int = 0) -> Optional[Face]:
    many_faces = get_many_faces(frame)
    if many_faces:
        try:
            return many_faces[position]
        except IndexError:
            return many_faces[-1]
    return None


def get_many_faces(frame: Frame) -> Optional[List[Face]]:
    try:
        return get_face_analyser().get(frame)
    except ValueError:
        return None


def find_similar_face(frame: Frame, reference_face: Face) -> Optional[Face]:
    many_faces = get_many_faces(frame)
    if many_faces:
        for face in many_faces:
            if hasattr(face, 'normed_embedding') and hasattr(reference_face, 'normed_embedding'):
                distance = numpy.sum(numpy.square(face.normed_embedding - reference_face.normed_embedding))
                if distance < roop.globals.similar_face_distance:
                    return face
    return None


def load_images_and_encodings(dataset_dir: str) -> List[numpy.ndarray]:
    print('[ROOP.ANALYSER] load images and encodings in dataset')
    images = [f for f in os.listdir(dataset_dir) if f.lower().endswith(('.png', '.jpg', '.jpeg'))]
    detector = MTCNN()

    for image_name in images:
        print('GET ENCODING FOR IMAGE', image_name)
        image_path = os.path.join(dataset_dir, image_name)
        image = Image.open(image_path)
        try:
            faces, prob_, landmarks_ = detector.detect(image, landmarks=True)
            if faces is not None:
               orientation = detect_face_orientation(faces, prob_, landmarks_)
               encoding = get_face_encoding(image)
               roop.globals.dataset_infos.append({'encoding': encoding, 'orientation': orientation, 'faces': faces, 'path': image_path})
        except Exception as e:
            print(f"An error occurred: {e} - Image: {image_name}")

    return roop.globals.dataset_infos


def get_similar_face_image_from_dataset(target_image_path: str, dataset_infos: List) -> Optional[str]:
    print('[ROOP.ANALYSER] search best face in dataset')
    detector = MTCNN()
    target_image = Image.open(target_image_path)
    closest_face = None
    default_closest_face = None

    try:
        default_closest_face = dataset_infos[0]['path']
        faces, prob_, landmarks_ = detector.detect(target_image, landmarks=True)
        if faces is None:
            print('Face not found - get first one from dataset', target_image_path, ': ', default_closest_face)
            return default_closest_face

        target_encoding = get_face_encoding(target_image)
        target_orientation = detect_face_orientation(faces, prob_, landmarks_)
        closest_face = get_closest_face_for_face_encoding(target_encoding, target_orientation, dataset_infos)
    except Exception as e:
        closest_face = default_closest_face

    if closest_face is None:
        closest_face = default_closest_face

    print('SIMILAR FACE FOR', target_image_path, ': ', closest_face)
    return closest_face


def calculate_encodings_distance(target_face_encoding: torch.Tensor, source_face_encoding: torch.Tensor) -> float:
    return (target_face_encoding - source_face_encoding).norm().item()


def calculate_faces_similarity(faces_distance: float, target_face_orientation: float, source_face_orientation: float) -> float:
    return faces_distance + (abs(target_face_orientation - source_face_orientation) / 360)


def get_face_encoding(target_image: Image) -> torch.Tensor:
    detector = MTCNN()
    resnet = InceptionResnetV1(pretrained='casia-webface').eval()
    aligned = detector(target_image)
    aligned = torch.unsqueeze(aligned, 0)
    return resnet(aligned).detach()


def get_closest_face_for_face_encoding(target_face_encoding: torch.Tensor, target_face_orientation: float, dataset_infos: List) -> Optional[str]:
    min_similarity = float('inf')
    closest_face = None
    for infos in dataset_infos:
        dataset_faces = infos['faces']
        if dataset_faces is not None:
            distance = calculate_encodings_distance(target_face_encoding, infos['encoding'])
            similarity = calculate_faces_similarity(distance, target_face_orientation, infos['orientation'])

            if similarity < min_similarity:
                min_similarity = similarity
                closest_face = infos['path']

    return closest_face

def detect_face_orientation(faces_: tuple[int, int, int, int], prob_: float, landmarks_: tuple[float, float]) -> float:
    for face, landmarks, prob in zip(faces_, landmarks_, prob_):
        if face is not None: # To check if we detect a face in the image
            angR = np_cos_angle(landmarks[0], landmarks[1], landmarks[2]) # Calculate the right eye angle
            angL = np_cos_angle(landmarks[1], landmarks[0], landmarks[2])# Calculate the left eye angle
            angle_diff = abs(angR - angL)
            return angle_diff
    return 180
